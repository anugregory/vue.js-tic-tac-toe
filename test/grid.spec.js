import {mount} from '@vue/test-utils'
import Grid from '../src/components/Grid'



describe('Grid.vue',()=>{
  it('testing_grid_fn',()=>{
    const wrapper = mount(Grid);
    expect(wrapper.contains('moves')).toBe(false);
    expect(wrapper.contains('winConditions')).toBe(false);
    expect(wrapper.find('div').text()).toBe("O's turn")
  });
});

describe('Grid.vue',()=>{
  it('testing_grid_status',()=>{
    const wrapper = mount(Grid);
    expect(wrapper.contains('activePlayer')).toBe(false);
    expect(wrapper.contains('gameStatus')).toBe(false);
  });
});

describe('Grid.vue',()=>{
  it('testing_grid_events',()=>{
    const wrapper = mount(Grid);
    wrapper.vm.$emit('win');
    wrapper.vm.$emit('freeze');
    wrapper.vm.$on('strike',()=>{
      expect(wrapper.contains('gameStatus')).toBe(true);
    })
  });
});


