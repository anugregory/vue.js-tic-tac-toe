import {mount} from '@vue/test-utils'
import Cell from '../src/components/Cell'




describe('Cell.vue',()=>{
  it('testing_cell_events',()=>{
    const wrapper = mount(Cell);
    wrapper.vm.$emit('strike');
    wrapper.vm.$on('clearCell',()=>{
      wrapper.text('mark').toBe('');
      wrapper.contains('freezed').toBe(false);
    });
  });
});
