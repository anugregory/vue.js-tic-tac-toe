# y

> Tic-Tac-Toe

Simple Vue.JS tic-tac-toe Implementation<live version is on www.anugregory.tk >


# install dependencies
-npm install



# serve with hot reload at localhost:8080
-npm run dev



# build for production with minification, <post build is enabled>
-npm run build


# run tests
- npm test


# live version on web
- www.anugregory.tk



